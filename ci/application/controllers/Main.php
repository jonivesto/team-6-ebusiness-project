<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Main extends CI_Controller {
	
	public function __construct()
	{
        parent::__construct(); 
        $this->load->helper('url');
        $this->load->model('Beach_model');
        $this->Beach_model = new beach_model;
	}
	
	/**
	 * Call model to get data from API.
	 * Load index view
	 */
	public function index()
	{
		// Prepare data
		$data = array();
		$data['locations'] = $this->Beach_model->get();
		$data['title'] = "Welcome";
		
		// Load views
		$this->load->view('header', $data);
		$this->load->view('main');
		$this->load->view('footer');
	}
	
	/**
	 * Call model to get data from API.
	 * Load beach view
	 */
	public function view($id = null){
		
		// Prepare data
		$data = array();
		$data['locations'] = $this->Beach_model->get();
		
		
		foreach($data['locations'] as $location){
			if($location['meas_id']==$id){
				$data['beach'] = $location;
			}
		}
		$data['title'] = $data['beach']['beach'];
		
		// Load views
		$this->load->view('header', $data);
		$this->load->view('beach', $data);
		$this->load->view('footer');
	}
	
}