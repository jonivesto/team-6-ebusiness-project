<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<div class="container col-md-12">
	<div class="row">
		
		
		<div class="col-md-6">
	<h1><?php echo $beach['beach'];?></h1>
	
	<p>Viimeisimmät tiedot ovat päivältä <?php echo $beach['day'];?>.<?php echo $beach['month'];?>.<?php echo $beach['year'];?></p>

	<p>Veden lämpotila oli silloin <?php echo $beach['water_temp'];?> astetta</p>
	
	</div>
	
	
	<div class="col-md-6">
		<img src="<?php echo base_url() . "assets/images/" . $beach['meas_id'] . ".jpg";?>"/>
	</div>
	
</div>
</div>
