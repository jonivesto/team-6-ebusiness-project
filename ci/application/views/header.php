<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title;?></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!--import css-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>" type="text/css" />
    </head>
    <body>
        
        
        
    <!--begin beach selector
    <div id="url" style="display:none;"><?php echo site_url('main/view/');?></div>
    <script type="text/javascript">
        var url = document.getElementById("url").innerHTML;
        function goTo(id){window.location=url+id;}
    </script>
    <select onchange="goTo(this.value);">
        <option selected disabled>Select beach</option>
        <?php //foreach ($locations as $item): ?>
            <option value="<?php //echo $item['meas_id'];?>"><?php// echo $item['beach'];?></option>
        <?php //endforeach; ?>
    </select>
    end beach selector-->
    
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/SZymd6r4wGg?start=80&rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                </div>
       <div class="row">
            <div class="col-12 text-center">
    <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Select beach
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      <?php $id = $this->uri->segment(1);
      
      foreach ($locations as $item): 
      $active = ($id == $item['meas_id'] ? 'active':' ');?>
    <a class="dropdown-item <?php echo $active;?>" href="<?php echo site_url($item['meas_id']);?>"><?php echo $item['beach'];?></a>

     <?php endforeach; ?>
     
  </div>
</div>

    
    
    <!--LINKKI ETUSIVULLE-->
    
    <a href="<?php echo site_url('main/index');?>" class="btn btn-info" id="front" role="button">Front page</a>
    
    
    </div>
    </div>
    </div>
