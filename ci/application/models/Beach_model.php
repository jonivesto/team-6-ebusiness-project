<?php class Beach_model extends CI_Model{
    
    /**
     * Get JSON data from the source.
     * Filter data only to contain latest records for each beach.
     * Convert to associative array and return.
     * 
     * @param		$year		year to get data from
     * @return		array       result data
     */
    public function get($year = 2017)
    {
        // Get data from url
		$source = 'https://api.ouka.fi/v1/swimming_water_quality_stats?year=eq.'.$year.'&order=meas_time.desc';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $source);
		$result = curl_exec($ch);
		$array = json_decode($result, true);
		curl_close($ch);
		// Get latest records for each beach
		$latest = array();
		$filtered = array();
		foreach($array as $item){
			if(!in_array($item['beach'],$latest)){
				array_push($latest,$item['beach']);
				array_push($filtered,$item);
			}
		}
		//return
		return $filtered;
    }
    
}